var app=angular.module("MyFirstApp",[]);
app.controller("ControladorTareas",function($scope)
	{
	$scope.tareas= [
				{texto:'ser super heroico con angularjs', hecho:true},
				{texto:'otra cosa', hecho:true}];
	$scope.nombre="arturo";
	$scope.agregarTarea=function()
	{
		$scope.tareas.push({texto:$scope.textoNuevaTarea, hecho:false});
		$scope.textoNuevaTarea='';
	};
	$scope.pendientes= function ()
	{
		var pendiente=0;
		angular.forEach($scope.tareas, function(tarea)
			{
				pendiente+= tarea.hecho?0:1;
			});
		return pendiente;		

	};
	$scope.eliminar= function()
	{
		var tareasViejas= $scope.tareas;
		$scope.tareas=[];
		angular.forEach(tareasViejas, function (tarea)
			{
				if(!tarea.hecho) $scope.tareas.Push(tarea); 
			});

	};
	});
